; ===========================================================
; ====================== Other Modules ======================
; ===========================================================

[module/mpd]
type = internal/mpd

; Host where mpd is running (either ip or domain name)
; Can also be the full path to a unix socket where mpd is running.
;;host = 127.0.0.1
;;port = 6600
;;password = mysecretpassword

; Seconds to sleep between progressbar/song timer sync
; Default: 1
interval = 1

; Available tags:
;   <label-song> (default)
;   <label-time>
;   <bar-progress>
;   <toggle> - gets replaced with <icon-(pause|play)>
;   <toggle-stop> - gets replaced with <icon-(stop|play)>
;   <icon-random>
;   <icon-repeat>
;   <icon-repeatone> (deprecated)
;   <icon-single> - Toggle playing only a single song. Replaces <icon-repeatone>
;   <icon-consume>
;   <icon-prev>
;   <icon-stop>
;   <icon-play>
;   <icon-pause>
;   <icon-next>
;   <icon-seekb>
;   <icon-seekf>
format-online = <label-song> <icon-prev> <toggle> <icon-next>
format-online-prefix = 
format-online-prefix-foreground = ${color.green}

;format-playing = ${self.format-online}
;format-paused = ${self.format-online}
;format-stopped = ${self.format-online}

; Available tags:
;   <label-offline>
format-offline = <label-offline>
format-offline-prefix = 
format-offline-prefix-foreground = ${color.red}

; Available tokens:
;   %artist%
;   %album-artist%
;   %album%
;   %date%
;   %title%
; Default: %artist% - %title%
label-song =  " %artist% - %title%"
label-song-maxlen = 25
label-song-ellipsis = true

; Available tokens:
;   %elapsed%
;   %total%
; Default: %elapsed% / %total%
label-time = %elapsed% / %total%
label-time-background = ${color.background-alt}
label-time-padding = 1

; Available tokens:
;   None
label-offline = " Offline"

; Only applies if <icon-X> is used
icon-play = 
icon-play-foreground = ${color.cyan}
icon-pause = 
icon-pause-foreground = ${color.cyan}
icon-stop = 
icon-stop-foreground = ${color.red}
icon-next = 
icon-next-foreground = ${color.cyan}
icon-prev = 
icon-prev-foreground = ${color.cyan}
icon-seekf = 
icon-seekb = 
icon-random = 
icon-repeat = 
icon-repeatone = 
icon-single = 
icon-consume =

; Used to display the state of random/repeat/repeatone/single
; Only applies if <icon-[random|repeat|repeatone|single]> is used
toggle-on-foreground = ${color.primary}
toggle-off-foreground = ${color.red}

; Only applies if <bar-progress> is used
;;bar-progress-width = 45
;;bar-progress-indicator = |
;;bar-progress-fill = ─
;;bar-progress-empty = ─

; ===========================================================

[module/wired-network]
type = internal/network
interface = eth0

[module/wireless-network]
type = internal/network
interface = wlp3s0

; ===========================================================


[module/powermenu]
type = custom/menu

; If true, <label-toggle> will be to the left of the menu items (default).
; If false, it will be on the right of all the items.
expand-right = true

; "menu-LEVEL-N" has the same properties as "label-NAME" with
; the additional "exec" property
;
; Available exec commands:
;   menu-open-LEVEL
;   menu-close
; Other commands will be executed using "/usr/bin/env sh -c $COMMAND"
menu-0-0 = " Reboot |"
menu-0-0-exec = menu-open-1
menu-0-1 = " Shutdown "
menu-0-1-exec = menu-open-2

menu-1-0 = " Back |"
menu-1-0-exec = menu-open-0
menu-1-1 = " Reboot "
menu-1-1-exec = systemctl reboot

menu-2-0 = " Shutdown |"
menu-2-0-exec = systemctl poweroff
menu-2-1 = " Back "
menu-2-1-exec = menu-open-0

; Available tags:
;   <label-toggle> (default) - gets replaced with <label-(open|close)>
;   <menu> (default)
; Note that if you use <label-toggle> you must also include
; the definition for <label-open>

format = <label-toggle><menu>

label-open = 
label-open-foreground = ${color.cyan}
label-open-padding = 1
label-close =  
label-close-foreground = ${color.red}
label-close-padding = 1

; Optional item separator
; Default: none
;label-separator = " | "
;label-separator-foreground = ${color.foreground}
;label-separator-background = ${color.background-alt}

;;label-open-foreground = ${color.foreground}
;;label-close-foreground = ${color.background}

; ===========================================================

[module/workspaces]
type = internal/xworkspaces

; Only show workspaces defined on the same output as the bar
;
; Useful if you want to show monitor specific workspaces
; on different bars
;
; Default: false
pin-workspaces = true

; Create click handler used to focus desktop
; Default: true
enable-click = true

; Create scroll handlers used to cycle desktops
; Default: true
enable-scroll = true

; icon-[0-9]+ = <desktop-name>;<icon>
; NOTE: The desktop name needs to match the name configured by the WM
; You can get a list of the defined desktops using:
; $ xprop -root _NET_DESKTOP_NAMES
icon-0 = 1;
icon-1 = 2;
icon-2 = 3;
icon-3 = 4;
icon-4 = 5;
icon-default = 

; Available tags:
;   <label-monitor>
;   <label-state> - gets replaced with <label-(active|urgent|occupied|empty)>
; Default: <label-state>
format = <label-state>

; Available tokens:
;   %name%
; Default: %name%
label-monitor = %name%

; Available tokens:
;   %name%
;   %icon%
;   %index%
; Default: %icon%  %name%
label-active = %icon%
label-active-foreground = ${color.pink}

; Available tokens:
;   %name%
;   %icon%
;   %index%
; Default: %icon%  %name%
label-occupied = %icon%
label-occupied-foreground = ${color.cyan}

; Available tokens:
;   %name%
;   %icon%
;   %index%
; Default: %icon%  %name%
label-urgent = %icon%
label-urgent-foreground = ${color.red}

; Available tokens:
;   %name%
;   %icon%
;   %index%
; Default: %icon%  %name%
label-empty = %icon%
label-empty-foreground = ${color.foreground}

label-active-padding = 1
label-urgent-padding = 1
label-occupied-padding = 1
label-empty-padding = 1

; ===========================================================

[module/temperature]
type = internal/temperature

; Seconds to sleep between updates
; Default: 1
interval = 0.5

; Thermal zone to use
; To list all the zone types, run 
; $ for i in /sys/class/thermal/thermal_zone*; do echo "$i: $(<$i/type)"; done
; Default: 0
thermal-zone = 0

; Full path of temperature sysfs path
; Use `sensors` to find preferred temperature source, then run
; $ for i in /sys/class/hwmon/hwmon*/temp*_input; do echo "$(<$(dirname $i)/name): $(cat ${i%_*}_label 2>/dev/null || echo $(basename ${i%_*})) $(readlink -f $i)"; done
; to find path to desired file
; Default reverts to thermal zone setting
;;hwmon-path = /sys/devices/platform/coretemp.0/hwmon/hwmon2/temp1_input
hwmon-path = /sys/devices/pci0000:00/0000:00:01.3/0000:01:00.0/hwmon/hwmon0/temp1_input

; Threshold temperature to display warning label (in degrees celsius)
; Default: 80
warn-temperature = 65

; Whether or not to show units next to the temperature tokens (°C, °F)
; Default: true
units = true

; Available tags:
;   <label> (default)
;   <ramp>
format = <ramp> <label>

; Available tags:
;   <label-warn> (default)
;   <ramp>
format-warn = <ramp> <label-warn>

; Available tokens:
;   %temperature% (deprecated)
;   %temperature-c%   (default, temperature in °C)
;   %temperature-f%   (temperature in °F)
label = %temperature-c%

; Available tokens:
;   %temperature% (deprecated)
;   %temperature-c%   (default, temperature in °C)
;   %temperature-f%   (temperature in °F)
label-warn = "%temperature-c%"
label-warn-foreground = ${color.red}

; Requires the <ramp> tag
; The icon selection will range from 0 to `warn-temperature`
; with the current temperature as index.
ramp-0 = 
ramp-1 = 
ramp-2 = 
ramp-3 = 
ramp-4 = 
ramp-foreground = ${color.cyan}

; ===========================================================

[module/title]
type = internal/xwindow

; Available tags:
;   <label> (default)
format = <label>
format-prefix = 
format-foreground = ${color.foreground}

; Available tokens:
;   %title%
; Default: %title%
label = " %title%"
label-maxlen = 30

; Used instead of label when there is no window title
label-empty = " Desktop"

; ===========================================================

